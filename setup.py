import os
from setuptools import setup

setup (
        name = "iesgallery",
        version = "1.1",
        author = "Florian Sachs",
        author_email = "florian.sachs@technikum-wien.at",
        packages = ["bic4ies",],
        license = "BSD",
        description = "A Gallery and Picture class using a xml file as container",
        long_description = "Module documentation can be found here http://www.schlof.net/iesgallery-doc/index.html",
        url = "https://bitbucket.org/sumpfgottheit/iesgallerycore/",
)
