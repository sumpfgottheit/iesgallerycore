# -*- coding: utf-8 -*-
__author__ = 'saf'

from bic4ies.gallery import _get_etree_element_text, Picture
from xml.etree.ElementTree import Element, SubElement
import os
import copy
import pytest
#
# Setup for noobs....
#

def touch(fname, times = None):
    with file(fname, 'a'):
        os.utime(fname, times)

def init_picture(t):
    filepath = str(t.join("picture"))
    touch(filepath)
    p = Picture(filepath)
    p.title = u"Ein hübsches Flugzeug"
    p.author = u"Florian Sachs"
    p.lat = "somelat"
    p.lon = "somelon"
    p.tags = ["Flugzeug","etwas fliegt"]
    p.idi = 12
    return p

def test_get_etree_element_text1():
    e = Element("picture")
    filepath = SubElement(e, "filepath")
    filepath.text = "mytext"
    assert _get_etree_element_text(e, "filepath") == "mytext"
    assert _get_etree_element_text(e, "nopath") == ''

def test_picture_attributes(tmpdir):
    p = init_picture(tmpdir)
    assert isinstance(p.tags, list)
    assert p.tagstring == "Flugzeug,etwas fliegt"
    assert len(p.tags) == 2
    assert _get_etree_element_text(p._element, "title") == u"Ein hübsches Flugzeug"
    assert _get_etree_element_text(p._element, "lon") == ""
    myfilepath = str(tmpdir.join("xx"))
    with pytest.raises(IOError):
        p.filepath = myfilepath

def test_filepath(tmpdir):
    pass

def test_xml_is_unicode(tmpdir):
    p = init_picture(tmpdir)
    assert type(p._xml) == unicode

def test_to_and_from_xml(tmpdir):
    p = init_picture(tmpdir)
    f = tmpdir.join("picturefile")
    p1 = copy.copy(p)
    filename = str(f)
    touch(filename)
    p1.filepath = filename
    xml = p1._xml
    p2 = Picture(str(f))
    p2._from_xml(xml)
    assert p1 == p2
    p2.title = u"Hällo"
    assert p1 != p2

