# -*- coding: utf-8 -*-
__author__ = 'saf'
import pytest
from bic4ies.gallery import *
from bic4ies.gallery import _search as internal_search
import os
import copy
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import ParseError

def touch(fname, times = None):
    with file(fname, 'a'):
        os.utime(fname, times)

def init_picture(t):
    filepath = str(t.join("picture"))
    touch(filepath)
    p = Picture(filepath)
    p.title = u"Ein hübsches Flugzeug"
    p.author = u"Florian Sachs"
    p.lat = "somelat"
    p.lon = "somelon"
    p.tags = ["Flugzeug","etwas fliegt"]
    p.idi = 12
    return p


def init_gallery(t):
    p = init_picture(t)
    p1 = copy.copy(p)
    p2 = copy.copy(p)
    p1_filepath = str(t.join('picture1.jpg'))
    touch(p1_filepath)
    p1.filepath = p1_filepath
    p2_filepath = str(t.join('picture2.jpg'))
    touch(p2_filepath)
    p2.filepath = p2_filepath
    filename = str(t.join('filanema.xml'))
    g = create_gallery(filename, "MyTitle")
    p1.tags = [u"eins", u"zwei", u"zwei"]
    p2.tags = [u"drei", u"föhre", u"zwei"]
    assert g.add_picture(p1) == 1
    assert g.add_picture(p2) == 2
    assert g._next_idi == 3
    return g

def test_get_gallery(tmpdir):
    filename = str(tmpdir.join('filanema.xxx'))
    with pytest.raises(GalleryFileError):
        get_gallery(filename)
    filename = str(tmpdir.join('filanema.xml'))
    with pytest.raises(GalleryFileError):
        get_gallery(filename)


def test_create_gallery(tmpdir):
    filename = str(tmpdir.join('filanema.xxx'))
    with pytest.raises(GalleryFileError):
        g = create_gallery(filename)
    filename = str(tmpdir.join('filanema.xml'))
    touch(filename)
    with pytest.raises(GalleryFileError):
        g = create_gallery(filename)
    filename = str(tmpdir.join('myfile.xml'))
    g = create_gallery(filename)
    assert type(g) == Gallery

def test_gallery_attributes(tmpdir):
    filename = str(tmpdir.join('filanema.xml'))
    g = create_gallery(filename, "MyTitle")
    assert type(g) == Gallery
    assert g.title == "MyTitle"
    g.title ="Anderer Title"
    assert g.title == "Anderer Title"

def test_add_first_picture(tmpdir):
    p = init_picture(tmpdir)
    filename = str(tmpdir.join('filanema.xml'))
    g = create_gallery(filename, "MyTitle")
    assert len(g.pictures) == 0
    g.add_picture(p)
    assert len(g.pictures) == 1
    assert g.pictures[0].idi == 1
    assert g._next_idi == 2
    assert len(g.pictures) == 1

def test_remove_picture(tmpdir):
    p = init_picture(tmpdir)
    p1 = copy.copy(p)
    filename = str(tmpdir.join('filanema.xml'))
    g = create_gallery(filename, "MyTitle")
    assert len(g.pictures) == 0
    idi = g.add_picture(p1)
    assert idi == 1
    assert len(g.pictures) == 1
    assert g.pictures[0].idi == 1
    assert g._next_idi == 2
    assert len(g.pictures) == 1
    with pytest.raises(PictureNotFoundError):
        g.remove_picture(2)
    with pytest.raises(ValueError):
        g.remove_picture("asb")
    p2 = g.remove_picture(1)
    assert p1 == p2

def test_tags(tmpdir):
    g = init_gallery(tmpdir)
    assert g.tags == set([u"eins", u"zwei", u"drei", u"föhre"])

def test_element(tmpdir):
    g = init_gallery(tmpdir)
    assert type(g._element) == Element
    assert g._element.attrib['title'] == "MyTitle"

def test_xml_is_unicode(tmpdir):
    g = init_gallery(tmpdir)
    assert type(g._xml) == unicode

def test_save(tmpdir):
    g = init_gallery(tmpdir)
    g.save()
    assert os.path.isfile(g.filepath)

def test_load(tmpdir):
    g = init_gallery(tmpdir)
    g.save()
    filepath = g.filepath
    k = get_gallery(filepath)
    k.load()
    assert g.pictures[0] == k.pictures[0]
    assert g.pictures[1] == k.pictures[1]

def test_xmlstring(tmpdir):
    g = init_gallery(tmpdir)
    x = u"Teststring"
    with pytest.raises(ParseError):
        g._from_xml(x)

def test_internal_search():
    assert internal_search("eins", "meinss") == True
    assert internal_search("eins", "zweis") == False

    assert internal_search("eins", ["meinss", "zweiss"]) == True
    assert internal_search("eins", ["kinss", "zweiss"]) == False

    assert internal_search(["eins", "zwei"], "meins") == True
    assert internal_search(["eins", "zwei"], "kins") == False

    assert internal_search(["eins", "zwei"], ["kins", "kzweins"]) == True
    assert internal_search(["eins", "zwei"], ["zins", "kins"]) == False

def test_search(tmpdir):
    g = init_gallery(tmpdir)
    g.save()
    assert len(g.search(x="YYY")) == 0
    assert len(g.search(author=u"Florian Sachs")) == 2
    assert len(g.search(author=u"Florian Od")) == 0
    assert len(g.search(tags=u"zwei")) == 2
    assert len(g.search(filepath=r"picture1.jpg$")) == 1
    assert len(g.search(tags=[u"eins", u"drei"], mode=MATCH_ALL)) == 2
    assert len(g.search(tags=[u"eins", u"drei"], mode=MATCH_ANY)) == 2
    assert len(g.search(tags=[u"föhre", u"drei"], mode=MATCH_ANY)) == 1
    assert len(g.search(tags=[u"föhre", u"drei"], mode=MATCH_ALL)) == 1
    assert len(g.search(tags=u"eins", mode=MATCH_ANY)) == 1
    assert len(g.search(tags=[u"eins",], mode=MATCH_ALL)) == 1

