# -*- coding: utf-8 -*-
"""
This module implements the peristance backend for a picture library. The peristance layer is a XML file, that is written
using xml.etree.ElementTree and the according methods.

For API users a completely object oriented Interface is provided.
"""

from xml.etree.ElementTree import Element, SubElement, tostring, XML, XMLParser
from os.path import isfile, abspath
import logging
import xml.dom.minidom
import re
import tempfile
import os
import codecs

LOGGER = logging.getLogger('iesgalleycore.core')

_get = lambda x: x if x else ""

MATCH_ANY = 'match_any'
MATCH_ALL = 'match_all'


class PictureNotFoundError(Exception):
    """Is emitted if a Picture could not be found"""
    pass


class GalleryFileError(Exception):
    """Something is wrong with the galleryfile"""
    pass


def _get_etree_element_text(root, name):
    """
    If there is a SubElement of root called 'name',
    return the text, else ''

    :param: root: root Element
    :type: root: xml.etree.ElementTree.Element
    :param name: name of the element to search for
    :type name: string
    """
    e = root.find(name)
    if e is not None:
        return unicode(e.text)
    else:
        return ""


def _touch(fname, times=None):
    """
    "touch" a given file

    :param fname: filename to touch
    :param times: utime to touch with
    """
    with file(fname, 'a'):
        os.utime(fname, times)


def _is_sequence(arg):
    """
    Return True if arg is a sequence type

    :param arg: some type
    :return: True or False
    """
    return (not hasattr(arg, "strip") and
        hasattr(arg, "__getitem__") or
        hasattr(arg, "__iter__"))


def _search(search_regex, search_here):
    """
    Search the search_regex within search_here. Works with sequence types and strings.
    Return True if anything could be found.

    :param search_regex: str or list which contains the search-regex(s)
    :param search_here: str of list within the search should be done
    :return: True of False
    """
    if _is_sequence(search_regex) and _is_sequence(search_here):
        for _search_regex in search_regex:
            for _search_here in search_here:
                if re.search(_search_regex, _search_here):
                    return True
    elif _is_sequence(search_regex) and not _is_sequence(search_here):
        for _search_regex in search_regex:
            if re.search(_search_regex, search_here):
                return True
    elif not _is_sequence(search_regex) and _is_sequence(search_here):
        for _search_here in search_here:
            if re.search(search_regex, _search_here):
                return True
    elif re.search(search_regex, search_here):
        return True
    return False


def create_gallery(filepath, title=None):
    """
    Create a new Gallery with the given filepath

    :param filepath: The filepath for the gallery
    :return: the created Gallery Object
    :raises: InvalidFileNameError - if filepath is not valid
    """
    filepath = abspath(filepath)
    if not filepath.endswith('.xml'):
        raise GalleryFileError("Filepath has to end with .xml")
    if isfile(filepath):
        raise GalleryFileError("File already exists")
    _touch(filepath)
    return Gallery(filepath, title)


def get_gallery(filepath):
    """
    get the Gallery with the given filepath

    :param filepath: The filepath for the gallery
    :return: the created Gallery Object
    :raises: AttributeError - if filepath is not valid
    :raises:
    """
    if not filepath.endswith('.xml'):
        raise GalleryFileError("Filepath has to end with .xml")
    if not isfile(filepath):
        raise GalleryFileError("Galleryfile does not exist")
    g = Gallery(filepath)
    g.load()
    return g


class Gallery(object):
    """Class that repesents a gallery.

    Gallery should never be called directly. To get or create a gallery, use the
    module functions

    :py:func:`get_gallery` and :py:func:`create_gallery()`

    When using :py:func:`get_gallery`, the :py:func:`Gallery.load` is called automatically.
    Remember to always use :py:func:`Gallery.save` to write the current contents of the gallery to disk.

    Add pictures to the gallery using the :py:func:`Gallery.add_picture` method.
    This method returns the **idi** of the picture, which is an int representing
    the internal.

    To remove a picture, call :py:func:`Gallery.remove_picture` with the idi of the picture to remove

    Search for pictures using the :py:func:`Gallery.search` method, but be aware, that i haven't implemented
    the method right now....
    """

    #: The name of the gallery
    title = None

    #: The path to the xml file, that contains the gallery
    filepath = None

    #: A list of Picture-objects
    _pictures = []

    #: The xml.etree.ElementTree representation of the gallery
    tree = None

    def __init__(self, filepath, title=None):
        self.title = title
        self.filepath = filepath
        self._pictures = []
        self.tree = None

    @property
    def _next_idi(self):
        """Return the next free idi"""
        if self._pictures is None or len(self._pictures) == 0:
            return 1
        else:
            return max([picture.idi for picture in self.pictures]) + 1

    def add_picture(self, picture):
        """Add a picture to the gallery. The idi is filled out by the gallery
        and returned

         :param picture: The picture to add to the gallery
         :type picture: Picture
         :returns: the idi of the added picture in the gallery
        """
        picture.idi = self._next_idi
        self._pictures.append(picture)
        return picture.idi

    @property
    def pictures(self):
        """Return the list of Pictures"""
        return self._pictures

    def remove_picture(self, idi):
        """Remove the picture with the given idi

        :param idi: the idi to be removed
        :type idi: int
        :raises: PictureNotFoundError - if no picture with the given idi has been found
        :raises: ValueError - if idi cound not be converted to an int
        """
        try:
            idi = int(idi)
        except ValueError:
            raise ValueError("idi could not be converted to an int: %s" % idi)
        r = None
        for i, picture in enumerate(self._pictures):
            if picture.idi == idi:
                r = picture
                del(self._pictures[i])
                break
        if r is None:
            raise PictureNotFoundError("No picture with idi %d found" % idi)
        return r

    @property
    def tags(self):
        """Return a set() all tags within the gallery

        :returns: set() of tags"""
        s = set()
        for picture in self.pictures:
            for tag in picture.tags:
                s.add(tag)
        return s

    def search(self, **kwargs):
        """Return a set of pictures, that conform to the given query.

        The query is a dict(), that contains attribute=>value pairs, that will be searched.
        The attribute is the Picture.property, that should be search, the value is the regex, that should be searched.

        If you provide a list as the value of the key=>value pair, a match will be positive, if any of the listitems
        match.

        It's also possible, to provide a **mode**. The mode is
            gallery.MATCH_ANY: return all pictures, that match at least one of the query-attributes
            gallery.MATCH_ALL: return pictures, that match ALL of the query attributes

        Internally, for every attribute a resultset is created.

        When using gallery.MATCH_ANY, the returned resultset contains the pictures, that match any,
        at least one, of the key=>value pairs.

        When using gallery.MATCH_ALL, the returned resultset contains the pictures, that match ALL,
        of the key=>value pairs.

        Example:
            find_by_attributes(author="Florian", tagstring='^only one tag$', mode=MATCH_ANY)
            returns all photos, where the author name contains the string "Florian" and the
            tagstring has to be exactely "only one tag"

        More examples can be found the test/test_gallery.py::test_search
        """
        mode = MATCH_ANY
        if 'mode' in kwargs:
            mode = kwargs['mode']
            kwargs.pop('mode')
        l = {}
        for key, value in kwargs.items():
            l[key] = set()
            for picture in self.pictures:
                if hasattr(picture, key):
                    attribut_value = getattr(picture, key)
                    if attribut_value is not None and _search(value, attribut_value):
                        l[key].add(picture)
        if mode == MATCH_ANY:
            return set.union(*l.values())
        else:
            return set.intersection(*l.values())

    @property
    def _element(self):
        """
        Property - Return a xml.etree.ElementTree.Element representation of the object

        :returns: xml.etree.ElementTree.Element
        """
        root = Element("gallery")
        root.attrib['title'] = self.title

        pictures = SubElement(root, "pictures")
        if self.pictures is not None and len(self.pictures) > 0:
            for picture in self.pictures:
                pictures.append(picture._element)

        return root

    @property
    def _xml(self):
        """
        XML representation of the current object

        First translates the object into an xml.etree.ElementTree.Element
        and uses the xml.dom.minidom to make the output pretty.
        The regex is necessary to avoid linefeeds before and after textelements
        """
        xml_string = tostring(self._element, encoding='UTF-8')
        xml_dom = xml.dom.minidom.parseString(xml_string)
        almost_pretty_xml = xml_dom.toprettyxml()
        regex = re.compile('>\n\s+([^<>\s].*?)\n\s+</', re.DOTALL)
        return unicode(regex.sub('>\g<1></', almost_pretty_xml.replace('<?xml version="1.0" ?>',
                                                                       '<?xml version="1.0" encoding="UTF-8"?>')))

    def _from_xml(self, xmlstring):
        """
        Fill the current object from the given xml string

        :param xmlstring: The xml repesentation of the picture
        :type xmlstring: UTF-8 String (type == unicode)
        :raises: ValueError - if the idi could not be converted to an int
        :raises: ParseError - if it's not a valid XML
        """
        root = XML(xmlstring.encode('utf-8'), parser=XMLParser(encoding='UTF-8'))
        self.title = root.attrib['title']

        pictures = root.find("pictures")
        self._pictures = []
        for picture in pictures.iterfind('picture'):
            pic = Picture(self.filepath)
            pic._from_element(picture)
            if pic.filepath == self.filepath:
                raise AttributeError("PictureFilepath and Galleryfilepath must be different")
            self._pictures.append(pic)

    def save(self):
        """
        Write the XML representation of the gallery to the file, given by self.filepath

        :raises: ValueError - if the idi could not be converted to an int
        :raises: ParseError - if it's not a valid XML
        """
        with codecs.open(self.filepath, "w", "utf-8-sig") as f:
            f.write(self._xml)
            f.close()

    def load(self):
        """Load the Gallery from the path given in self.filepath. The whole gallery is save as
        UTF-8 file with a BOM. (utf-8-sig)

        :raises: ValueError - if Picture-ID is invalid
        :raises: xml.etree.ElementTree - if XML-Parsing did not work
        """
        with codecs.open(self.filepath, "r", "utf-8-sig") as f:
            xmlstring = f.read()
            f.close()
            self._from_xml(xmlstring)


class Picture(object):
    """
    The picture class represents a, nona, picture. It's important, that **every** attribute has to be a
    valid UTF-8 string.

    Let's repeat it together: *I WILL ALWAYS USE UNICODE FOR ATTRIBUTES*

    The constructor checks, if filepath is a valid file or will raise an IOError
    """

    #: str - Filepath to the picture. Use the filepath property and not _filepath directly
    _filepath = None

    #: str - The great title of the photo
    title = None

    #: str - The author of the photo
    author = None

    #: list - A list containing the tags for the photo
    tags = []

    #: str - latitude
    lat = None

    #: str - longitude
    lon = None

    #: int - a unique identifier for the photo. It's not called 'id' as 'id'
    #: has a special meaning in python.
    #:Use Gallery.next_id to get a valid id to fill here
    idi = None

    def __init__(self, filepath, title=None, author=None, taglist=None, lat=None, lon=None):
        """
        Create a new Picture with the given parameters. The filepath has to be a **valid** filepath
        """
        self.filepath = filepath
        self.title = title
        self.author = author
        self.tags = taglist if taglist is not None else []
        self.lat = lat
        self.lon = lon

    @property
    def tagstring(self):
        """return a string with all tags concatenated by a comma"""
        return ','.join(self.tags) if self.tags is not None else ""

    @property
    def filepath(self):
        """Return the filepath"""
        if not os.path.isfile(self._filepath.decode('utf-8')):
            raise IOError("Picture file %s not found" % self._filepath)
        return self._filepath

    @filepath.setter
    def filepath(self, filepath):
        """set the internal attribute self._filepath
        :raises: IOError - if filepath is not a file
        """
        if filepath is None:
            return
        if not isfile(abspath(filepath)):
            raise IOError("Picture file %s not found" % filepath)
        else:
            self._filepath = filepath

    @property
    def _element(self):
        """
        Property - Return a xml.etree.ElementTree.Element representation of the object

        :raises: IOError - if the filepath is not a valid file
        :raises: AttributeError - if filepath is None
        :returns: xml.etree.ElementTree.Element
        """
        if self.filepath is None:
            raise AttributeError("Filepath cannot be None")

        root = Element("picture")
        root.attrib['idi'] = str(self.idi)

        filepath = SubElement(root, "filepath")
        filepath.text = self.filepath

        title = SubElement(root, "title")
        title.text = _get(self.title)

        author = SubElement(root, "author")
        author.text = _get(self.author)

        taglist = SubElement(root, "tags")
        if self.tags is not None:
            for tag in self.tags:
                tmp = SubElement(taglist, "tag")
                tmp.text = tag

        location = SubElement(root, "location")
        lat = SubElement(location, "lat")
        lat.text = _get(self.lat)

        lon = SubElement(location, "lon")
        lon.text = _get(self.lon)

        return root

    def _from_element(self, element):
        """
        Fill the current object from the given element

        :param element: The element repesentation of the picture
        :type elementg: Element
        :raises: ValueError - if the idi could not be converted to an int
        """
        try:
            self.idi = int(element.attrib['idi'])

            self.filepath = _get_etree_element_text(element, "filepath")
            self.title = _get_etree_element_text(element, "title")
            self.author = _get_etree_element_text(element, "author")
            location = element.find("location")
            self.lat = _get_etree_element_text(location, "lat")
            self.lon = _get_etree_element_text(location, "lon")

            tags = element.find("tags")
            self.tags = []
            for tag in tags.iterfind('tag'):
                if tag.text is not None:
                    self.tags.append(_get(tag.text))
        except ValueError:
            raise ValueError("idi could not be converted to int")

    def _from_xml(self, xmlstring):
        """
        Fill the current object from the given xml string

        :param xmlstring: The xml repesentation of the picture
        :type xmlstring: UTF-8 String (type == unicode)
        :raises: ValueError - if the idi could not be converted to an int
        """
        try:
            root = XML(xmlstring.encode('utf-8'), parser=XMLParser(encoding='UTF-8'))
            self._from_element(root)
        except ValueError:
            raise ValueError("idi could not be converted to int")

    @property
    def _xml(self):
        """
        XML representation of the current object

        First translates the object into an xml.etree.ElementTree.Element
        and uses the xml.dom.minidom to make the output pretty.
        The regex is necessary to avoid linefeeds before and after textelements
        """
        xml_string = tostring(self._element, encoding='UTF-8')
        xml_dom = xml.dom.minidom.parseString(xml_string)
        almost_pretty_xml = xml_dom.toprettyxml()
        regex = re.compile('>\n\s+([^<>\s].*?)\n\s+</', re.DOTALL)
        return unicode(regex.sub('>\g<1></', almost_pretty_xml.replace('<?xml version="1.0" ?>',
                                                                       '<?xml version="1.0" encoding="UTF-8"?>')))

    def __eq__(self, other):
        """Check if we have the same attributes"""
        if type(self) == type(other) and \
            self.idi == other.idi and \
            self.title == other.title and \
            self.author == other.author and \
            self.tagstring == other.tagstring and \
            self.lat == other.lat and \
            self.lon == other.lon and \
            self.filepath == other.filepath:
            return True

    def __unicode__(self):
        ret = "\n".join([
                "Title    : %s" % self.title,
                "Filepath : %s" % self.filepath,
                "Author   : %s" % self.author,
                "IDI      : %s" % self.idi,
                "Location : %s / %s" % (self.lat, self.lon),
                "Tags     : %s" % ", ".join(self.tags)
        ])
        return unicode(ret)

if __name__ == '__main__':
    p = Picture()
    p.title = u"Ein hübsches Flugzeug"
    p.author = u"Florian Sachs"
    p.lat = "somelat"
    p.lon = "somelon"
    p.tags = ["Flugzeug", "etwas fliegt"]
    p.idi = 12

    tmpdir = tempfile.mkdtemp()
