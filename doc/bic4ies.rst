bic4ies Package
===============

Simple Usage of the module:

.. code-block:: python

    # -*- coding: utf-8 -*-
    from bic4ies.gallery import create_gallery, get_gallery, Picture

    # Let's create a new gallery
    mygallery = create_gallery("/Users/saf/tmp/mygallery.xml")

    # Set a title
    mygallery.title = u"Eine schöne Gallerie"

    # Create a picture
    p = Picture("/Users/saf/tmp/mypic.jpg")

    # Set some attributes
    p.title = u"Ein Urlaubsphoto"
    p.tags = [ u"schön", u"Nebel" ]

    # Add the picture to the gallery
    idi = mygallery.add_picture(p)
    # The idi is the internal id of mygallery for the picture

    # Write the xml file
    mygallery.save()

    # Now let's recreate the gallery
    anothergallery = get_gallery("/Users/saf/tmp/mygallery.xml")
    print anothergallery.title
    print len(anothergallery.pictures)
    print anothergallery.pictures[0].title


:mod:`bic4ies` Package
----------------------

.. automodule:: bic4ies.__init__
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`gallery` Module
---------------------

.. automodule:: bic4ies.gallery
    :members:
    :undoc-members:
    :show-inheritance:

