.. IES Gallery documentation master file, created by
   sphinx-quickstart on Wed Jun  6 18:36:05 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to IES Gallery's documentation!
=======================================

This module serves as a backend for the gallery, that has been programmed
for *Information Engineering and Security* on the `FH Technikum Wien
<http://www.technikum-wien.at>`_

The group of geniuses consists of this 4 wonderful individuals, that are  
also known as the *Real Fantastic Four*

* **Florian Sachs**
* **Gabriel Pendl**
* **Muris Ahmovic**
* **Serdar Gül**

How to get the source code
--------------------------

The module is hosted on bitbucket

  https://bitbucket.org/sumpfgottheit/iesgallerycore

Just clone it via::

  git clone git@bitbucket.org:sumpfgottheit/iesgallerycore.git

Install it via pip
------------------

Better try this out in an virtualenv::

    pip install -e git+https://bitbucket.org/sumpfgottheit/iesgallerycore.git#egg=bic4ies

or do the::

    python setup.py install

dance in the projectdir

.. note::

    After installing via pip in an virtualenv, i couldn't import in in
    ipython, whereas python worked...

What this module is all about
-----------------------------

IESGallery is about to make Flickr look like Meganoobs. It provides an object oriented interface to a picture gallery.
The information about gallery and pictures is stored in an xml file.


Contents
--------

.. toctree::
   :maxdepth: 5

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

