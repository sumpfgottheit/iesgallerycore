Information about the bic4ies module
====================================

API-Documentation
-----------------

.. toctree::
   :maxdepth: 4

   bic4ies

PEP8
----

The code has been pep8'ed using the following parameters::

    pep8 --max-line-length=120

.. note::

    pep8 can be incorporated into `PyCharm <http://www.jetbrains.com/pycharm/>`_  as external tool.

    Goto :menuselection:`Preferences --> External Tools --> Add` and go on:

    .. list-table:: 
       :header-rows: 1

       * - Field
         - Value
         - Example
       * - Name
         - Name will be displayed in PyCharm Tools Menu
         - pep8
       * - Program
         - Absolute path to the pep8 script
         - /opt/local/Library/Frameworks/Python.framework/Versions/2.7/bin/pep8
       * - Parameters
         - pep8-script parameters
         - --max-line-length=120 $FilePath$ 
       * - Working directory
         - Let's insert a macro..
         - $FileDir$

    Add the correct output filter using :menuselection:`Output Filters...
    --> Add` and fill the following values:

    .. list-table::
        :header-rows: 1

        * - Field
          - Value
          - Example
        * - Name
          - Name it as you like
          - pep8-std-output-filter
        * - Description
          - something
          - FooBar
        * - Regular Expression to match output
          - PyCharm will interpret it an makes it clickable
          - $FILE_PATH$\:$LINE$\:$COLUMN$\:.*

    You now can pep8 via :menuselection:`Tools --> pep8`. If you did put
    it in a group, it can be found via :menuselection:`Tools -->
    Groupname --> pep8`


PyLint
------

Next to pep8, pylint has been run over the whole code with the messages 
`E0202 <http://pylint-messages.wikidot.com/messages:e0202>`_ , 
`C0301 <http://pylint-messages.wikidot.com/messages:c0301>`_ and  
`C0103 <http://pylint-messages.wikidot.com/messages:c0103>`_ disabled. The
pylintrc file is in the projectdir of the module::

    pylint --rcfile=pylintrc bic4ies/gallery.py


.. note::

    Like pep8, pylint can also be incorporated into `PyCharm <http://www.jetbrains.com/pycharm/>`_.
    
    Goto :menuselection:`Preferences --> External Tools --> Add` and go on:

    .. list-table:: 
       :header-rows: 1

       * - Field
         - Value
         - Example
       * - Name
         - Name will be displayed in PyCharm Tools Menu
         - pylint
       * - Program
         - Absolute path to the pep8 script
         - /opt/local/Library/Frameworks/Python.framework/Versions/2.7/bin/pylint
       * - Parameters
         - pylint parameters
         - --rcfile=$ProjectFileDir$/pylintrc $FilePath$
       * - Working directory
         - It's mandatory to use / as working dir
         - /

    Add the correct output filter using :menuselection:`Output Filters...
    --> Add` and fill the following values:

    .. list-table::
        :header-rows: 1

        * - Field
          - Value
          - Example
        * - Name
          - Name it as you like
          - pylint-std-output-filter
        * - Description
          - something
          - MoreFoo
        * - Regular Expression to match output
          - PyCharm will interpret it an makes it clickable
          - $FILE_PATH$:$LINE$:.*

py.test
-------

The test coverage is just awesome. As testrunner, `py.test <http://pytest.org/>`_ has been used.
Just run in in the procjectdirectory::

  (iesgallerycore)mbp:~/devel/virtualenvs/iesgallerycore/iesgallerycore-app
  saf$py.test
  ================ test session starts ================
  platform darwin -- Python 2.7.3 -- pytest-2.2.4
  collected 16 items 
  
  bic4ies/test/test_gallery.py ...........
  bic4ies/test/test_picture.py .....
  
  ================ 16 passed in 0.17 seconds ================


